# Terraform

Ceci est une introduction au cours Terraform.

Plan du cours :

1. [IaC](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/1.%20IaC.md?ref_type=heads)
2. [Découvrir Terraform](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/2.%20D%C3%A9couvrir%20Terraform.md?ref_type=heads)
3. [Fonctionnement de Terraform](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/3.%20Fonctionnement%20de%20Terraform.md?ref_type=heads)
4. [Installer Terraform](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/4.%20Installer%20Terraform.md?ref_type=heads)
5. [Configurer Terraform pour Azure](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/5.%20Configurer%20Terraform%20pour%20Azure.md?ref_type=heads)
6. [Providers.tf](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/6.%20Providers.tf.md?ref_type=heads)
7. [Terraform init](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/7.%20Terraform%20Init.md?ref_type=heads)
8. [Terraform plan](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/8.%20Terraform%20Plan.md?ref_type=heads)
9. [Terraform apply](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/9.%20Terraform%20Apply.md?ref_type=heads)
10. [Terraform graph](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/10.%20Terraform%20Graph.md?ref_type=heads)
11. [Terraform destroy](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/11.%20Terraform%20Destroy.md?ref_type=heads)
12. [Organiser les fichiers de l'infrastructure](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/12.%20Organiser%20les%20fichiers%20de%20l'infrastructure.md?ref_type=heads)
13. [Variables](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/13.%20Variables.md?ref_type=heads)
14. [Structures conditionnelles](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/14.%20Structures%20conditionnelles.md?ref_type=heads)
15. [Structures itératives](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/15.%20Structures%20it%C3%A9ratives.md?ref_type=heads)
16. [Output/Sorties](https://gitlab.com/fyc-finops/terraform/-/blob/main/cours/16.%20Output-Sorties.md?ref_type=heads)